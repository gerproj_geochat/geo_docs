# Documentação Geo Server
Este repositório contem a documentação de projeto do Geo Server.

O código fonte está armazenado em repositório separado. O link pode ser encontrado abaixo.

## Veja o demo
Acesse a aplicação em: https://unifei-geo-chat.herokuapp.com

## Codigo fonte
O repositorio com o código fonte está disponivel em: https://gitlab.com/gerproj_geochat/geoserver_api


## Estrutura de diretorios
A pasta dist contem os documentos em suas versões atuais e em formatos de arquivo de facil abertura, como PDF e PNG.

A pasta src contem as versões editaveis dos documentos em formato .doc, entre outros de programas propietarios.

## Apresentado como projeto da disciplina de Gerencia de Projetos
Turma 2017.1

Universidade Federal de Itajuba

Prof. Adler


### Autores:
- Joao Paulo Motta Oliveira Silva, 24482
- Leandro de Lima Duarte, 24433
- Victor Rodrigues da Silva, 31054
